package pmonitoreo.backend.cohabitante;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Cohabitante {
    private final String name;
    private final List<Object[]> historial;
    private final Path pathCsv = Paths.get("src\\csv\\reportes.csv");

    public Cohabitante(String name){
        this.name = name;
        this.historial = new ArrayList<>();
        int nHistorial = 5;
        this.leerCsvHistorial(nHistorial);
    }

    public List<Object[]> getHistorial(){
        return this.historial;
    }
    private void leerCsvHistorial(int n){
        int i=0;
        try{
            BufferedReader br = Files.newBufferedReader(pathCsv);
            String linea;
            while((linea = br.readLine()) !=null){
                Object[] datosLinea =linea.split(",");
                if(i<n  && (datosLinea[0].toString().equals(this.name))) {
                    this.historial.add(datosLinea);
                    i++;
                }
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }



}
