package pmonitoreo.frontend.asanitario;

import pmonitoreo.backend.asanitario.ASanitario;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class GuiAsanitario implements MouseListener {
    JFrame frame ;
    JPanel panel;
    JPanel panelCohabitante;
    JPanel reportes;
    JPanel tablaPanel;
    JPanel notificaciones;
    JPanel alertas;
    JLabel labelInfoAS;
    JTabbedPane panelPestanias;
    JTable jtableReportes;
    JTable jtableNotificaciones;
    JLabel notificacionX;
    DefaultTableModel modeloTR;
    DefaultTableModel modeloTN;
    DefaultTableModel modeloTHistorialC;
    JLabel labelFiltros ;
    JPanel opcionesReportes;
    Object[] filtrosReportes ={"Todos","Verde","Amarillo","Rojo"};
    JComboBox<Object> comboBoxFiltro;
    JPanel panelHistorialC ;
    JTable jtableHistorialC;
    JLabel labelEstadoC;
    JPanel panelInfoReporte;
    JLabel infoReporte;
    Object[] estadosCohabitante ={"","Verde","Amarillo","Rojo"};
    JComboBox<Object> cbEstadosC;
    JPanel panelEstadoC;
    JButton btnGuardar, btnAtras;
    JPanel reporteC = new JPanel();
    ASanitario as;

    public  GuiAsanitario(ASanitario as){
        this.frame = new JFrame("Agente Sanitario");
        this.as = as;
        this.init();
    }

    private void init(){
        JPanel infoASpanel = new JPanel();
        JPanel pestanias = new JPanel();
        this.panelPestanias = new JTabbedPane(JTabbedPane.TOP);
        this.panel = new JPanel();
        this.reportes = new JPanel();
        this.notificaciones = new JPanel();
        this.alertas= new JPanel();
        this.tablaPanel = new JPanel();

        this.panelCohabitante = new JPanel();


        this.frame.setSize(500,700);
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.frame.setLocationRelativeTo(null);

        this.labelInfoAS = new JLabel();
        this.labelInfoAS.setText("Agente Sanitario: "+ this.as.getName());
        infoASpanel.add(labelInfoAS);

        LayoutManager layout = new BoxLayout(panel, BoxLayout.Y_AXIS);
        this.panel.setLayout(layout);
        this.panel.add(infoASpanel);

        this.panelPestanias.addTab("Reportes",null, this.reportes, null);
        this.panelPestanias.addTab("Mis Notificaciones",null, this.notificaciones,null);
        this.panelPestanias.addTab("Alertas",null, this.alertas,null);

        pestanias.add(this.panelPestanias);

        this.setPanelReportes();
        this.setPanelNotificaciones();

        this.frame.getContentPane().add(panel);
        frame.pack();
        frame.setResizable(false);
        frame.setVisible(true);
    }
    private void setPanelReportes(){

        this.llenarTablaReportes();

        JScrollPane scrollReportes = new JScrollPane();
        scrollReportes.setViewportView(jtableReportes);
        this.tablaPanel.add(scrollReportes);
        this.panel.add(this.panelPestanias);
        this.setFiltrosReportes();
        this.crearPanelReporteC();
        this.reportes.setLayout(new BoxLayout(reportes,BoxLayout.Y_AXIS));
        this.reportes.add(this.labelFiltros);
        this.reportes.add(this.opcionesReportes);
        this.reportes.add(this.tablaPanel);
        this.reportes.add(this.reporteC);
        this.reporteC.setVisible(false);
    }

    private void setFiltrosReportes(){
        this.labelFiltros = new JLabel("Fitrar por : ");
        this.comboBoxFiltro = new JComboBox<>(this.filtrosReportes);
        this.opcionesReportes = new JPanel();
        opcionesReportes.add(comboBoxFiltro);
    }
    private  void setPanelNotificaciones(){
        JPanel tablaN = new JPanel();
        JScrollPane scrollNotificaciones = new JScrollPane();
        this.notificacionX = new JLabel();
        JPanel panelNotificacionX = new JPanel();
        panelNotificacionX.add(notificacionX);
        this.llenarTablaNotificaciones();
        this.llenarNotificacionX(0);
        scrollNotificaciones.setViewportView(jtableNotificaciones);
        tablaN.add(scrollNotificaciones);
        this.notificaciones.setLayout(new BoxLayout(notificaciones, BoxLayout.Y_AXIS));
        this.notificaciones.add(panelNotificacionX);
        this.notificaciones.add(tablaN);
    }
    private void llenarNotificacionX(int index){
        this.notificacionX.setText(this.getNotificacion(index));
    }

    private String getNotificacion(int index) {
       return "<html> <p>" + "COHABITANTE: " + this.jtableNotificaciones.getValueAt(index, 0)+ "<br>"
                + "SOLICITUD: " + this.jtableNotificaciones.getValueAt(index, 1)+ "<br>"
                + "DESCRIPCION: " + this.jtableNotificaciones.getValueAt(index, 2)+ "<br>"
                + "ATENDIDO: " +this.jtableNotificaciones.getValueAt(index, 3)+ "<br>"
                + "FECHA:  " +this.jtableNotificaciones.getValueAt(index, 4) + "</p></html>";
    }

    private void llenarTablaNotificaciones(){
        this.jtableNotificaciones = new JTable();
        modeloTN = new DefaultTableModel();
        for (Object c: as.getDatosNotificaciones()){
            modeloTN.addColumn(c);
        }
        this.jtableNotificaciones.setModel(modeloTN);
        for (Object  r:  as.getNotificaciones()){
            modeloTN.addRow((Object[])r);
        }
        this.jtableNotificaciones.setModel(modeloTN);
        this.jtableNotificaciones.addMouseListener(this);
    }
    private void llenarTablaReportes(){
        this.jtableReportes = new JTable();
        this.modeloTR = new DefaultTableModel();
        for (Object c: this.as.getDatosReportes()){
            modeloTR.addColumn(c);
        }
        this.jtableReportes.setModel(modeloTR);

        for (Object  r:  as.getReportesCohabitantes()){
            modeloTR.addRow((Object[])r);
        }
        this.jtableReportes.setModel(modeloTR);
        jtableReportes.addMouseListener(this);
    }
    private  void  llenarTablaHistorial(String nameC){
        this.modeloTHistorialC = new DefaultTableModel();
        for (Object c: this.as.getDatosReportes()){
            this.modeloTHistorialC.addColumn(c);
        }
        this.jtableHistorialC.setModel(modeloTHistorialC);

        for (Object [] r: this.as.getHistorialCohabitante(nameC)){
            modeloTHistorialC.addRow(r);
        }
        this.jtableHistorialC.setModel(this.modeloTHistorialC);
        this.jtableHistorialC.addMouseListener(this);
    }
    private void retornarReportes(){
        this.reporteC.setVisible(false);
        this.labelFiltros.setVisible(true);
        this.opcionesReportes.setVisible(true);
        this.tablaPanel.setVisible(true);
    }


    private void iniciarBotonoesRC(){
        this.btnAtras = new JButton("Atras");
        this.btnGuardar = new JButton("Guardar");
        this.btnAtras.addMouseListener(this);
        this.btnGuardar.addMouseListener(this);
    }
    private void crearPanelReporteC() {
        this.panelInfoReporte = new JPanel();
        this.infoReporte = new JLabel();
        this.panelInfoReporte.add(infoReporte);

        this.panelHistorialC = new JPanel();
        this.jtableHistorialC = new JTable();
       this.reporteC.setLayout(new BoxLayout(this.reporteC, BoxLayout.Y_AXIS));

        this.iniciarBotonoesRC();

        this.panelEstadoC = new JPanel();

        this.labelEstadoC = new JLabel("Asignar Estado: ");
        this.cbEstadosC = new JComboBox<>(this.estadosCohabitante);
        this.panelEstadoC.add(this.labelEstadoC);
        this.panelEstadoC.add(cbEstadosC);

        this.panelHistorialC.add(jtableHistorialC);

        this.reporteC.add(this.btnAtras);
        this.reporteC.add(this.panelInfoReporte);
        this.reporteC.add(this.panelEstadoC);
        this.reporteC.add(this.panelHistorialC);
        this.reporteC.add(this.btnGuardar);
    }

    private String getReporteX(int index){
        return "<html> <p>" + "COHABITANTE: " + this.jtableHistorialC.getValueAt(index, 0)+ "<br>"
                + "SINTOMAS: " + this.jtableHistorialC.getValueAt(index, 1)+ "<br>"
                + "ESTADO: " +this.jtableHistorialC.getValueAt(index, 2)+ "<br>"
                + "FECHA:  " +this.jtableHistorialC.getValueAt(index, 3) + "</p></html>";
    }

    private void setPanelCohabitante(int fila){
        String nombreC = (String) jtableReportes.getValueAt(fila,0 );
        this.llenarTablaHistorial(nombreC);
        this.llenarReporteX(0);
        this.tablaPanel.setVisible(false);
        this.labelFiltros.setVisible(false);
        this.opcionesReportes.setVisible(false);
        this.reporteC.setVisible(true);
    }
    private void llenarReporteX(int index){
        this.infoReporte.setText(this.getReporteX(index));
    }
    private void guardaReporte(){

    }
    @Override
    public void mouseClicked(MouseEvent e) {
        int fila;
        if(e.getSource() == this.btnGuardar){
            this.guardaReporte();
        }
        else if(e.getSource() == this.btnAtras){
            this.retornarReportes();
        }else if( e.getSource() == this.jtableNotificaciones){
             fila = jtableReportes.rowAtPoint(e.getPoint());
            this.llenarNotificacionX(fila);

        }else if (e.getSource() == this.jtableReportes) {
             fila = jtableReportes.rowAtPoint(e.getPoint());
             this.setPanelCohabitante(fila);
        }else if(e.getSource() ==  this.jtableHistorialC){
            fila = this.jtableHistorialC.rowAtPoint(e.getPoint());
            this.llenarReporteX(fila);
        }
    }
    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
