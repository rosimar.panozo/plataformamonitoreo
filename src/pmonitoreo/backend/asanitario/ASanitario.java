package pmonitoreo.backend.asanitario;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ASanitario{
    private final String nombre;
    private final List<Object[]> reportesCohabitantes;
    private final List<Object> datosReportes;
    private  final List<Object> datosNotificaciones;
    private final List<Object[]> notificaciones;
    private List<Object[]> historialCohabitante;
    private final Path csvReportes = Paths.get("src\\csv\\reportes.csv");
    private final Path csvNotificaciones = Paths.get("src\\csv\\notificacionesAS.csv");


    public  ASanitario(String nombre){
        this.datosReportes = new ArrayList<>();
        this.reportesCohabitantes  = new ArrayList<>();
        this.notificaciones = new ArrayList<>();
        this.datosNotificaciones = new ArrayList<>();
        this.nombre = nombre;
        this.leerCsvReportes();
        this.leerCsvNotificaciones();
    }

    private void  leerCsvReportes(){
        int i=0;
        try{
            BufferedReader br = Files.newBufferedReader(csvReportes);
            String linea;
            while((linea = br.readLine()) !=null){
                Object[] datosLinea =linea.split(",");
                if(i<1) {
                    for (Object dato : datosLinea) {
                        this.datosReportes.add(dato);
                        i++;
                    }
                }else {
                    this.reportesCohabitantes.add(datosLinea);
                }
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    private void  leerCsvNotificaciones(){
        int i=0;
        try{
            BufferedReader br = Files.newBufferedReader(csvNotificaciones);
            String linea;
            while((linea = br.readLine()) !=null){
                Object[] datosLinea =linea.split(",");
                if(i<1) {
                    for (Object dato : datosLinea) {
                        this.datosNotificaciones.add(dato);
                        i++;
                    }
                }else {
                    this.notificaciones.add(datosLinea);
                }
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }
    private void leerCsvHistorial(String nameC){
        this.historialCohabitante = new ArrayList<>();
        int i=0;
        try{
            BufferedReader br = Files.newBufferedReader(csvReportes);
            String linea;
            while((linea = br.readLine()) !=null){
                Object[] datosLinea =linea.split(",");
                int nHistorial = 5;
                if(i< nHistorial && (datosLinea[0].toString().equals(nameC))) {
                    this.historialCohabitante.add(datosLinea);
                    i++;
                }
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public List<Object> getDatosReportes(){
    return this.datosReportes;
    }
    public List<Object> getDatosNotificaciones(){
        return this.datosNotificaciones;
    }
    public List<Object[]> getReportesCohabitantes() {
        return this.reportesCohabitantes;
    }
    public List<Object[]> getNotificaciones(){ return  this.notificaciones;}

    public List<Object[]> getHistorialCohabitante(String name){
        this.leerCsvHistorial(name);
        return this.historialCohabitante;
    }


    public String getName(){
        return this.nombre;
    }
}
